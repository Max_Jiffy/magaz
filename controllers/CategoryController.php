<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 26.12.2017
 * Time: 19:09
 */

namespace app\controllers;
use app\controllers\CustomController;
use app\models\Category;
use app\models\Product;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Url;

class CategoryController extends CustomController
{
    public function actionView()
    {

        $id = Yii::$app->request->get('id');


        $category = Category::findOne(['id' => end($id)]);

        $ids = [];

        CustomController::printr($id);

        if($category->parentId == 0)
        {
            CustomController::printr($category);
            foreach ($category->children as $idChildren)
            {
                $ids[]= $idChildren->id;
            }
        }
        else{
            $ids[] = end($id);
        }

        $query = Product::find()->where(['categoryId'=> $ids]);




        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);


        return $this->render('view', compact('dataProvider'));
    }
}