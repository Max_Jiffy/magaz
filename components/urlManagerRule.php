<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 27.12.2017
 * Time: 11:04
 */

namespace app\components;

use app\controllers\CustomController;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use yii\helpers\Url;

class urlManagerRule extends BaseObject implements UrlRuleInterface
{


    //Формирует ссылки в заданном виде
    public function createUrl($manager, $route, $params)
    {



        // Разбераем Категории
        if ($route === 'category/view')
        {

            $model = Category::find()->where(['id' => $params['id']])->with('parent')->one();

            if ($model->parent) {
                $url = 'category/' . $model->parent->url .'/'.$model->url;
            } else{
                $url = 'category/' . $model->url;
            }

            // прибовляем параметры
            if(count($params) > 1)
            {
                foreach ($params as $key => $value)
                {
                    if($key != 'id')
                    {
                        $url = $url . '?'. $key . '=' .$value;
                    }
                }
            }

            return $url;
        }

        return false;
    }

    //Разбирает входящий URL запрос, преобразует ссылки произвольного вида  в нужный для Yii2
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        $urls = explode("/", $pathInfo);

        if ($urls[0] == 'category')
        {
            unset($urls[0]);

            $id = [];

            if (count($urls) > 1)
            {
                $cat = Category::find()
                    ->where(['url' => reset($urls)])
                    ->one();

                $model = Category::find()
                    ->where(['url' => end($urls), 'parentId' => $cat->id])
                    ->asArray()
                    ->one();

            } else{
                $model = Category::find()
                    ->where(['url' => $urls])
                    ->asArray()
                    ->one();
            }

            $id['id'][] = $model['id'];

            return ['category/view' , $id];
        }
        // Приабразовываем для админки
        else if ($urls[0] == 'admin')
        {
            $pathInfoAdmin = $request->url;

            $urls = explode("=", $pathInfoAdmin);

            $id['id'] = array_pop($urls);

            return [$pathInfo, $id];
        }

        return false;
    }
}
