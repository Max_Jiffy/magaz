<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 19.01.2018
 * Time: 0:09
 */
use yii\helpers\Url;
use yii\helpers\Html;
$img = $model->getImage();
?>



<li class="product-item">
    <div class="wrap-product-img">
        <a href="detail.html"><img src="<?= $img->getUrl('300x300') ?>" alt="img" /></a>
        <span class="saleoff style1">sale off</span>
    </div>
    <div class="wrap-product-content">
        <h4><a href="detail.html"><?= $model->title ?></a></h4>

        <span class="price">
        <?php

        switch ($model->sale) {
            case 1 :
                echo '<del><span class="amount">' .$model->price. '</span></del>
                      <ins><span class="amount">' .$model->salePrice.'</span></ins>';
                break;

            case 0 :
                echo '<ins><span class="amount">'.$model->price.'</span></ins>';

        }

        ?>
        </span>
        <div class="star-rating"></div>
    </div>
    <div class="wrap-links">
        <a href="#">Add to Cart</a>
        <a href="#">Wish List</a>
    </div>
</li>
